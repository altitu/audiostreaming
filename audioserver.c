#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <arpa/inet.h>

#include "./syr2-audio-lib/audio.h"

#include <signal.h>

#define MAXCLIENT 100 //la limite souple du nombre maximal de client (-1) que l'on peut servir simultanement
#define LOCALPORT 1234 //le port pour la reception des données
#define MSGLENGTH 1024 //la taille maximale d'un buffer de reception
#define SAMPLEPERBUFFER 100 //le nombre de sample par buffer
#define NOMULTIPLE 1 //permet de se limiter à "un client = une chanson". Si mis à 1: plusieurs client avec la même ip peuvent écouter des chansons différentes (c'est mieux)
#define ECOMODE 0 //desactivé. si mis à 1: il permet de ne pas faire tourner le programme à 100% (on le laissera à 0 pour des performances optimales)


typedef struct __db_client {
	/*A completer*/
	int   numclient; //ne doit pas depasser MAXCLIENT - 1.
	char  ipadress[16];
	unsigned short port;
	char  *filename; //= NULL; //pour la chanson
	int   sample_rate;
	int   sample_size;
	int   channels;
	int   status; // = -1;
	int   statusbis; //=0 sauf si buffer preparé car manquant
	int   numberofsamples; //à la fin
	char  buffer[2][SAMPLEPERBUFFER];
	int   fd_read;
	int   filter; //peut être dans le client
	struct __db_client * next; //liste circulaire
	} db_client;


db_client * signal_arg = NULL;
int * signal_arg2 = NULL;

void die(const char* text){
	perror(text);
	exit(1);
}

/**
 * pour la gestion d'erreurs conditionnelles
 */
void dieif(int boolexpr, const char* text){
	if (boolexpr > 0){
		die(text);
	}
}


/**
 * Création d'une nouvelle socket
 */
int * creer_socket(const int domain, const int type, int protocol){
	int * ret = (int*)malloc(sizeof(int));
	*ret = socket(domain,type,protocol);
	dieif(*ret == -1,"can't create socket()");
	return ret;
}

/**
 * Binder la socket
 */
void binder_socket(int *fd, struct sockaddr_in *server, int port){
	server->sin_family = AF_INET;
	server->sin_addr.s_addr = htonl(INADDR_ANY);
	server->sin_port = htons((unsigned short) port);
	int ret = bind(*fd, (struct sockaddr *) server, sizeof(struct sockaddr_in));
	dieif(ret != 0,"probleme lors du bind()");	
}

/**
 * initialise une structure
 */
void init_struct(db_client *temp){
	memset(temp, 0, sizeof(db_client));
	temp->status = -2; //donne le sample en court d'envoit, ou -2 si rien n'est en cours d'envoit.
	temp->next = temp;
}


/**
 * mise à jour de la liste client avec l'adresse ip : raffraichît l'état ou decide d'ajouter un client
avec 
	char  ipadress[15] + '\0' = 16
	char  *filename = NULL; //pour la chanson
	int   sample_rate;
	int   sample_size;
	int   channels;
et le role (-1 pour rien, >=0 pour n° de sample si envoit, 
 * 
 */

int dejapresent(db_client *temp, char *ipadress){ //renvoit -1 si non présent, sinon le n° du client
	//s'il n'y a qu'un seul element dans la liste circulaire:
	if (temp->numclient == temp->next->numclient && temp->next->numclient == temp->next->next->numclient){
		if(strncmp(temp->ipadress,ipadress,15) == 0){
			return temp->numclient;
		} else {
			return -1;
		}
	}

	while (temp->numclient != temp->next->numclient){
		if(strncmp(temp->ipadress,ipadress,15) == 0){
			return temp->numclient;
		} else {
			temp->next = temp;
		}
	}
	return -1;
}

/**
 * Suppression d'un élement de db_client
 */

void suppr_struct(db_client *temp){
	//retrouver le morceau precedent, et le morceau suivant
	db_client *next = temp->next;
	db_client *previous = temp->next;
	while (previous->next != temp){
		previous = previous->next;
	}
	previous->next = next;
	/*
	if(temp->filename == NULL){ //le flag
		free(temp->fd_read );
		free(temp);
		return;
	}*/
	free(temp->filename);
	close(temp->fd_read);
	free(temp);	
}

/*
 * Suppresion de tout les élements d'une liste circulaire de db_temp
 */
void suppr_all_struct(db_client *temp){
	while (temp->next != temp){
		suppr_struct(temp->next);
	}
	suppr_struct(temp);
}	

/*
 *
 */
db_client * ajoute_struct(db_client *temp){
	db_client * ajout = (db_client *)malloc(sizeof(db_client));
	memset(ajout, 0,sizeof(db_client));
	ajout->next = temp->next;
	temp->next = ajout;
	return ajout;
}

//renvoit:
//0: maj de la var status
//1: temp deleted -> on doit envoyer un "fin" au client -> inutile
//2: nouveau client ajouté ou autre à voir
/*
void maj_etat(db_client *temp, char *ipadress, int status){ //si status = -1 -> to be deleted; etc
	strncpy(ipadress,temp->ipadress,16); //TODO: à vérifier (le 16e est de toute façon nulle = '\0')
	temp->status = status;
	if (dejapresent(ipadress) == -1){
		if (status == -1) {
			//gerer ici pour envoyer un datagram "fin" selon l'adresse contenue dans temp;
			suppr_struct(temp); //TODO coder suppr_temp avec un free();
			
		}
	} else {
	#if NOMULTIPLE == 1 //TODO voir XMACRO pour les if
		//TODO si on veut pouvoir avoir de multiple chanson sur un même client
	#endif
	}
}*/

/**
 * renvoit le plus grand numéro déja présent "numclient" de la liste
 * NOTE: ne modifie pas temp
 */
int max_numclient_struct(db_client *temp){
	//on ne peut pas compter simplement le nombre de structures car il y a des structures uniquement pour le traitement des erreurs
	//on doit donc trouver le plus grand numéro et le retourner	
	int numclient = 0;
	db_client * head = temp;
	do{
		if (head->numclient > numclient) {
			numclient = head->numclient;
		}
		head = head->next;
	} while(head->next != head);
	return numclient;
}

db_client * recherche_struct(db_client *temp, char *ipadress,  unsigned int port, char *filename){
	db_client *head = temp;
	do
	{
		if ((strncmp(head->ipadress,ipadress,15) == 0) && (strncmp(head->filename,filename,MSGLENGTH) == 0)){
			return NULL;
		}
		//avec l'option NOMULTIPLE mis à 1, on ne peut pas avoir deux fois la même ip par client
		#if NOMULTIPLE == 1
		if ((strncmp(head->ipadress,ipadress,15) == 0)){
			return NULL;
		}
		#endif

		db_client *result = ajoute_struct(head);
		int number = max_numclient_struct(head) + 1;
		if (number > MAXCLIENT) { //car on compte les clients à partir de 0
			db_client *error = ajoute_struct(head);
			strncpy(error->ipadress, ipadress, 15); 
			error->ipadress[15] = '\0';
			error->port = port;
			error->statusbis = -2; //proceed enverra une erreur liée aux clients si statusbis = -2
		}
		result->numclient = number;
		strncpy(result->ipadress, ipadress, 15);
		result->ipadress[15] = '\0';
		result->port = port;
		result->filename = filename;
		return result;
	
		head = head->next;	
	} while (head->next != head);	// pour traiter head si un seul element
	
}

void maj_struct(db_client *temp, char *ipadress, unsigned short port, char *filename/*, int status*/, int statusbis, int filter){

	db_client *selection = recherche_struct(temp, ipadress, port, filename);

	switch(statusbis) {
		case 0: //on recherche une concordance ip et filename => existe alors (NULL) -> erreur | n'existe pas (creer par fonction recherche_struct et mis avec l'ip et port et le filename et le numclient) -> on cree le fd_read et on remplit les champs, et on ne remplit pas le buffer

			if (selection == NULL) {
				//TODO: traiter l'erreur
				//ajouter une structure avec l'ip et le port etc
				// mettre le statusbis à -1
				//faire en sorte que proceed comprenne qu'il s'agit d'une erreur.
				db_client *newtemp = ajoute_struct(temp);
				strncpy(newtemp->ipadress, ipadress, 15);
				newtemp->ipadress[16] = '\0';
				newtemp->port = port;
				newtemp->status = 0; //creation
				newtemp->statusbis = -1;
				return;
			}

			fprintf(stdout, "acceptation du client %s ayant demandé la chanson %s",selection->ipadress, selection->filename);
			int sample_rate, sample_size, channels;
			int fd_read = aud_readinit(filename, &sample_rate, &sample_size, &channels);
			selection->sample_rate = sample_rate;
			selection->sample_size = sample_size;
			selection->channels = channels;
			selection->status = 0; // = -1;
			selection->statusbis = 0; //=0 sauf si buffer preparé car manquant
			selection->numberofsamples = -1; //dependra du read() dans proceed
			selection->fd_read = fd_read;
			selection->filter = filter; //peut être dans le client
//	maj_struct(temp, inet_ntoa(from.sin_addr), ntohs(from.sin_port), buff, &sample_rate, &sample_size, &channels, 0/*le premier buffer*/, fd_read);
		
			break;

		default:
			if (selection == NULL){
				suppr_all_struct(temp);
				die("une erreur dans l'interpretation du buffer client s'est produite");
			}
			selection->statusbis = statusbis;
			break;
			//on a juste à mettre à jour statusbis
			
	}
}

/////////////////////fonction necessaire
/*
char * sous_char(char *temp, int deb, int maxfin){
	dieif(deb>maxfin,"probleme dans sous_char");
	for (int i=deb; i<=maxfin; i++){
		if (i == maxfin) {
			temp[maxfin] = '\0';
		}
		if (temp[i] == '\0'){
			char * buffer = (char *)malloc((i-deb)sizeof(char));
			return strncat(buffer, temp+deb, i); 
		}
	}
	//la fonction return dans tout les cas
}
*/


////////////////////////////////////////

/**
 * Le premier filtre: réduire le volume
 */
void filter_volume(db_client *temp, float value){
	for (int i=0; i<SAMPLEPERBUFFER; i++){
		temp->buffer[0][i] =  ( ( (int16_t) (temp->buffer)[0][i]) * value);
	}
}

//void //TODO: faire le read pour proceed();

/**
 * parser de reception de requete client
 * sur un buffer
 * 
 */
void parser(int fd, struct sockaddr_in *from, socklen_t *flen, db_client *temp){
	char msg[MSGLENGTH];
	//struct sockaddr_in from;
	//socklen_t flen = sizeof(struct sockaddr_in);
	int len = recvfrom(fd, msg, sizeof(msg), 0,(struct sockaddr*) from, flen);
	dieif(len == 0, "len = 0 dans parser"); //TODO: à vérifier
	/*
	 * liste de requêtes supportées:
	 * exemple: M filename avec l'espace comptant
	 * MF0filename avec M pour musique F pour filtre (ici n°0 -> int8_t) et filename pour la chanson
	 * B0filename pour redemander le premier buffer: 0 est un int32_t.
	 * 
	 * 
	 * 
	 */
	msg[MSGLENGTH-1] = '\0';

	char *buff;
	int32_t filter;
	int32_t sample_num;

	switch (msg[0]){
		case 'M': //musique
			switch (msg[1]){
				case ' ': 
					 buff = (char*)malloc(strlen(msg+2)+1);
					 strncpy(buff, msg+2, MSGLENGTH);
					 maj_struct(temp, inet_ntoa(from->sin_addr), ntohs(from->sin_port), buff, /*0 le premier buffer,*/ 0/*statusbis*/, 0/*n° de filtre*/);
					 break; 
				case 'F': //TODO: filter
					 strncpy((char*) &filter, msg+2, 4);
					 buff = (char*)malloc(strlen(msg+6)+1);
					 strncpy(buff, msg+6, MSGLENGTH);
					 maj_struct(temp, inet_ntoa(from->sin_addr), ntohs(from->sin_port), buff, /* 0le premier buffer,*/ 0/*statusbis*/, filter/*n° de filtre*/);
					 break;
			}
			break;
		case 'B': //[1B 'char'][4B 'int32'][max 1019B 'char *']
			 strncpy((char *) &sample_num,msg+1,4); //on a recupéré l'int
			 //char *buff = sous_char(msg, 5, MSGLENGTH);
			 buff = (char *)malloc(strlen(msg+5)+1); //1 pour '\0'
			 strncpy(buff,msg+5,MSGLENGTH); //on a récupéré filename
			 maj_struct(temp, inet_ntoa(from->sin_addr), ntohs(from->sin_port), buff, /*0,*/ sample_num/*&((int32_t) atoi(sous_char(sample_num, 1, 5) ) )*/, 0 );
			 //remplir le buffer de la struct par les samples manquantes, et mettre le n° de buffer qui devait être rattrapé dans statusbis
			 break;
	}
		
}

/**
 * envoyer une trame construite au buffer selon
 */
void preparer_envoit_datagram(int fd, struct sockaddr_in *sock, const int domain, char ipadress[16], int port, int32_t length, char *msg){

	int err;
	
	sock->sin_family = domain;
	sock->sin_port = htons((unsigned short) port);
	sock->sin_addr.s_addr = inet_addr(ipadress);

	err = sendto(fd, msg, length, 0, (struct sockaddr *) sock, sizeof(struct sockaddr_in) );
	dieif(err == -1, "probleme lors de l'envoit d'un datagram");

	fprintf(stderr,"an error occured: could not send to %s on port %d this paquet: \n", ipadress, port);
	write(STDOUT_FILENO, msg, length);
}

/*
 * prepare le message pour envoit
 * Si buffernumero est négatif alors il s'agit d'une erreur qui va être envoyé au client
 * un datagram contient:
 * [n° du buffer (ou de l'erreur) 4B][fin ? 1B][nombre de sample envoyé 4B][longueur du titre 1B][titre ?B][buffer de samples 2*SAMPLEPERBUFFER B]
 */
char * preparer_msg(int32_t buffernumero,int8_t fin, int32_t channels, int32_t numberofsample, char *filename, char buffer[2][SAMPLEPERBUFFER]){ //copy on write
	//malloc ici
	int filenamelength;
	if (filename == NULL) {
		filenamelength = 0; //on évite ainsi une erreur de segmentation
	} else {
		filenamelength = strlen(filename);
	}
	char *msg = (char *)malloc(sizeof(int32_t)*3+sizeof(int8_t)*2+filenamelength+(SAMPLEPERBUFFER*2));
	((int32_t *) msg)[0]     = buffernumero;
	((int8_t *)  msg)[4]     = fin;
	((int32_t *) (msg+1))[1]  = channels;
	((char *)    msg)[9]     = numberofsample; //casting fun
	((int8_t *)  msg)[4*3+1] = filenamelength;
	strncpy(msg+3*4+2,filename,filenamelength); //on ne copiera pas le '\0'// le filename est necessaire pour distinguer deux client d'une même machine.
	memcpy(msg+(3*4+2+filenamelength), buffer, SAMPLEPERBUFFER*2);
	return msg;
}

/**
 * traite les envois de paquets sur tout les éléments de la liste
 */
void proceed(int fd, struct sockaddr_in *sock, db_client *temp){
	db_client *mem = temp;
	do
	{ //on a fait un tour


		if (temp->status == -1){
			suppr_struct(temp);
			continue;
		}
		char *msg;
		int8_t fin;
		int filenamelength;
		if (temp->filename == NULL) { //flag
			filenamelength = 0; //on évite ainsi une erreur de segmentation
			return; //TODO: tester
		} else {
			filenamelength = strlen(temp->filename);
		}

		//envoyer le buffer, incrementer status de un sauf si statusbis est !=0 auquel cas on remet juste statusbis à 0
		if (temp->statusbis >=0 ){ //demande de buffer
			temp->numberofsamples = read(temp->fd_read, (void *) temp->buffer, (temp->sample_size)*SAMPLEPERBUFFER);
			if (temp->statusbis == 0) {
				temp->status +=1;
			} //sinon nous sommes en mode de renvoit de buffer
			if (temp->numberofsamples < SAMPLEPERBUFFER ){
				fin = 1;
			}

/*
			while (read_result == sample_size){
				read_result = read(fd[0], (void *) buffer, sample_size);
				if (write(fd[1],(void *) buffer,sample_size) < sample_size)
					perror("probleme lors de l'envois vers les hauts parleurs");
		
			}

*/
			int32_t choix;
			if (temp->statusbis >0){ //renvoit de buffer
				choix = temp->statusbis;
			} else { //on veut simplemet un buffer à la suite
				choix = temp->status;
			}
			msg = preparer_msg((int32_t) choix, fin, (int32_t) temp->channels, (int32_t) temp->numberofsamples, temp->filename, temp->buffer);
			temp->statusbis = 0;

			preparer_envoit_datagram(fd, sock, AF_INET, temp->ipadress, temp->port,2*4+2+filenamelength+(temp->numberofsamples), msg);
			if (fin == 1){
				suppr_struct(temp); //on supprime la structure une fois qu'on a finit.
			}
		} else { //situation anormale //TODO
			switch (temp->statusbis){
				case -1:
					msg = preparer_msg((int32_t) temp->statusbis, -1,  (int32_t) temp->channels, 0, temp->filename, temp->buffer);
					temp->statusbis = 0;
					preparer_envoit_datagram(fd, sock, AF_INET, temp->ipadress, temp->port,2*4+2+filenamelength+(temp->numberofsamples), msg);
					break;
				case -2:
					msg = preparer_msg((int32_t) temp->statusbis, -1,  (int32_t) temp->channels, 0, temp->filename, temp->buffer);
					temp->statusbis = 0;
					preparer_envoit_datagram(fd, sock, AF_INET, temp->ipadress, temp->port,2*4+2+filenamelength+(temp->numberofsamples), msg);
					break;
				default:
					suppr_all_struct(temp);
					die("statusbis out of bound");
			}
		}
		temp = temp->next;
	} while(temp->next != mem);
}

void signalhandler(int num){
	if (signal_arg != NULL || signal_arg2 != NULL) {
	suppr_all_struct(signal_arg->next); //le flag fermera le socket car fd_read sera fermé.
	free(signal_arg2);
	} else {
		write(STDERR_FILENO,"\npas le temps de supprimer la liste circulaire\n",47);
	}
	die("\nau revoir!");
}


int main(int argc, char *argv[]) {
	signal(SIGINT, signalhandler); //pour la fermeture du programme
	//on malloc car on va se retrouver avec une liste chaînée circulaire dont tout les élements seront free()
	db_client *temp = (db_client *)malloc(sizeof(db_client));
	init_struct(temp);	
	
	//note: les sockets sont des files descriptors, c'est à dire codés sur des int.
	int * sock; //le fd associé à la socket
	//sock pour la reception, client(mais ici sock aussi) pour l'envoit

	//char msg2send[MSGLENGTH]; //pour le message à envoyer
	//char msg2rcv[1024]; //pour le message à recevoir du client <-inutile

	sock = creer_socket(AF_INET,SOCK_DGRAM,0);	
	signal_arg2 = sock; //pour la suppression future
	signal_arg = temp; //on ne peut le mettre avant car on a besoin de sock dans fd_read
	struct sockaddr_in sock_reception;
	binder_socket(sock, &sock_reception, LOCALPORT); //la fonction s'occupe de la conversion vers  (struct sockaddr *)
	socklen_t sock_reception_length = 0;
	


	struct sockaddr_in sock_client;


	fd_set readfs, writefs;

	while(1) //TODO
	{
		
		
		FD_ZERO(&readfs);
		FD_ZERO(&writefs);
		FD_SET(*sock, &readfs);
		FD_SET(*sock, &writefs);

		/*struct timeval chrono;
		chrono.tv_sec = 5;
		chrono.tv_usec = 0;*/

		int ret = select(*sock + 1, &readfs, &writefs, NULL, NULL/*&chrono*/);
		dieif(ret<0,"can't select()");

		/*
		if(ret == 0)
		{
			ici le code si la temporisation (dernier argument) est écoulée (il faut bien évidemment avoir mis quelque chose en dernier argument).
		}
		*/

        	if(FD_ISSET(*sock, &readfs)){
			//on reçoit donc on parse le message
			//du coup on ajoute l'ip si elle n'est pas présente. en fait ça dépend d'un paramètre.
			//ensuite on parcours la liste de db_client et on envoit ce qui est demandé là o`u c'est demandé.
			parser(*sock, &sock_reception, &sock_reception_length, temp);

			/* des données sont disponibles sur le socket */
			/* traitement des données */
		}
		if(FD_ISSET(*sock, &writefs)){
			#if ECOMODE ==1
			usleep(10);
			#endif
			proceed(*sock, &sock_client, temp);
			/* des données sont disponibles sur le socket */
			/* traitement des données */
		}
	}

}

