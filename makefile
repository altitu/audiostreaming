CC = gcc
CFLAGS = -Wall -Wextra -pedantic -O0
DEBUG=-g
EXEC1=audioserver

all: $(EXEC1)

audioserver: audioserver.o audio.o
	$(CC) -o $@ audioserver.o audio.o

audio.o:
	$(CC) -o $@ -I./syr2-audio-lib/ -c ./syr2-audio-lib/audio.c $(CFLAGS)

audioserver.o: 
	$(CC) -o $@ -c audioserver.c $(CFLAGS)

%.o: %.c
	$(CC) -c ($<)

lire:
	chmod +x ./audioserver
	xterm "./audioserver" & 
	xterm "padsp ./audioclient 127.0.0.1 ./syr2-audio-lib/test.wav" &

clean:
	rm -rf *.o

mrproper: clean
	rm -rf $(EXEC1)
